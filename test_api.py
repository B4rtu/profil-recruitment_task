import movies
import pytest
class Tests():
    def test_filter1(self,api):
        result = api.filter(1,'Scott')
        assert result == [('Gladiator', 'Ridley Scott'), ('Alien', 'Ridley Scott'), ('Blade Runner', 'Ridley Scott')]
    def test_compare(self,api):
        result = api.compare('runtime','The Hunt','Gran Torino')
        assert result == [('Gran Torino', '116 min')]
    def test_highscores(self,api):
        result = api.highscores()
        assert result==[('Gone with the Wind', '238 min'), ('The Dark Knight', '$533,316,061'), ('Boyhood', 172.0),
                        ('Boyhood', 381.0),('The Shawshank Redemption', 9.3), ('Amadeus', 8)]
    def test_filter2(self, api):
        result = api.filter(2, 'DiCaprio')
        assert result == [('Inception', 'Leonardo DiCaprio, Joseph Gordon-Levitt, Ellen Page, Tom Hardy'),
                          ('The Departed', 'Leonardo DiCaprio, Matt Damon, Jack Nicholson, Mark Wahlberg'),
                          ('Django Unchained', 'Jamie Foxx, Christoph Waltz, Leonardo DiCaprio, Kerry Washington'),
                          ('The Wolf of Wall Street', 'Leonardo DiCaprio, Jonah Hill,'
                                                      ' Margot Robbie, Matthew McConaughey'),
                          ('Shutter Island', 'Leonardo DiCaprio, Mark Ruffalo, Ben Kingsley, Max von Sydow')]
    def test_filter3(self,api):
        result = api.filter(6,'Polish')
        assert result == [('Gods', 'Polish'), ('The Hunt', 'Danish, English, Polish')]
    def run_all_tests(self,api):
        self.test_compare(api)
        self.test_filter1(api)
        self.test_filter2(api)
        self.test_filter3(api)
        self.test_highscores(api)
