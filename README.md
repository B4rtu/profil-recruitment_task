# Profil-recruitment_task

Uruchomienie programu:
W odpowiedniej lokalizacji uruchomić Python console.
I następnie skrypt.

\>>> execfile('movies.py')

Utworzyć obiekt klasy api zawartej w skrypcie. Np.

\>>> a = api()

Uzzupełnić wartości w odpowiednich kolumnach  

\>>> a.populate()

Zagadnienia:

1) Sortowanie według kolumn:
Metoda sorting z odpowiednim parametrem zwraca posortowaną tabele. Parametr to 
string opisujący jeden lub wiele kolumn. Nazwa kolumny musi być z dużych liter 
a przy dwoch kolumnach należy wpisać dwie kolumny z przecinkiem pomiędzy nimi 
np. 'TITLE, [CAST]'. W przypadku kolumny CAST pokrywa się ona z jedną z funkcji 
SQLite stąd konieczność użycia nawiasów kwadratowych. Przykład.

\>>>a.sorting('[CAST],RUNTIME')

2)Filtrowanie

Metoda --> filter

Parametry --> option(int 1-5) , string 

option = 1 - filtuj według rezysera , string - Nazwisko,Imię lub oba w "uszach"
option = 2 - filtruj według aktora , string - jak wyżej
option = 3 - filtruj nominowanych ale nie laureatów Oscara , string = None 
option = 4 - 100M$< Box Office Earnings , string = None
option = 5 - 80%< wygranych nominacji, string = None
option = 6 - filtuj według języka, string = pisany z dużej litery język(ang)
przykłady. 

\>>>a.filter(1,'Scorsese') >>> a.filter(2,'Bale') >>>a.filter(3)
\>>> a.filter(6,"English")

3)porównywanie filmów według wartości w podanej kolumnie

Nazwa metody --> compare

Parametr--> option: 'BO', 'imdb', 'awards_won', 'runtime', 

Parametry --> m1,m2 - nazwa filmu w stringu 

Przykład. \>>>a.compare('runtime',"Gran Torino","Gone With The Wind")

4) Dodawanie pozycji do bazy danych


Metoda --> add_movie

parametr --> title - nazwa filmu w stringu. 

Przykład. \>>> a.add_movie('Kobiety Mafii')

5) Najwyższe wyniki 

Metoda--> highscores

\>>>a.highscores()
 
 Testowanie

\>>>execfile('test_api.py')

Utworzyć obiekt klasy Tests np.

\>>>t = Tests()

Dla metod jako parametr trzeba użyć poprzednio utowrzonego obiektu klasy api.

Żeby uruchomić wszystkie testy.

\>>>t.run_all_tests(a)

Inne testy.

\>>>t.test_compare(a)

\>>>t.test_filter1(a)

\>>>t.test_filter2(a)

\>>>t.test_filter3(a)

\>>>t.test_highscores(a)

Jeśli jako parametru, do którejś z metod, potrzebujemy wpisać string zawierający 
CAST, to należy umieścić go w nawiasach kwadratowych, ze względu na zbierzność 
nazw z jedną z funkcji SQLite. 







