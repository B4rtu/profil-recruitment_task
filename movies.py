import requests
import sqlite3
import re

class api():
    def __init__(self):
        self.db = sqlite3.connect('movies.sqlite') # get table from sqlite file

    def populate(self):

        sel = self.db.execute("SELECT * FROM movies")
        i = 0
        for row in sel:                                     # or every tuple in db get movie description
            api = requests.get('http://www.omdbapi.com/?t={}&apikey=464fa30b'.format(row[1]))  # by matching the title
            values = []
            j = api.json()                                              # make it into json
            for tag in j:                                               # get values of every key in json
                values.append(j[tag])                                   # and put it into list
            sql = ''' INSERT OR REPLACE INTO movies( ID, TITLE , YEAR, RUNTIME, GENRE, DIRECTOR, [CAST], WRITER, 
            LANGUAGE, COUNTRY, AWARDS, IMDb_Rating, IMDb_votes, BOX_OFFICE) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,? ) '''
            self.db.execute(sql, (
            i, values[0], values[1], values[4], values[5], values[6], values[8], values[7], values[10], values[11],
            values[12], values[16], values[17], values[-4]))    # execute insert into on table with specific values
            i = i + 1

    def sorting(self,string):

        sort = self.db.execute("SELECT {},TITLE FROM movies ORDER BY {} DESC".format(string,string)).fetchall()
        for each in sort:
            print(each)         # select given columns from table and return
        return sort


    def filter(self,option=None, string2=None):

        if option == 6:                             # Filter by language
            filtered = self.db.execute("SELECT TITLE, {} FROM movies ".format('LANGUAGE'))
            result = []                             # list of tuples appended later with filtered  information
            for movie in filtered:                  # iterate through all movies
                if string2 in movie[1]:             # for every row find usage of string in the language column
                    print("{}   {}".format(movie[0],movie[1]))
                    result.append(movie)
            return result

        if option == 1:                             # likewise with director
            filtered = self.db.execute("SELECT TITLE, {} FROM movies ".format('DIRECTOR'))
            result = []
            for movie in filtered:
                if string2 in movie[1]:
                    print("{}   {}".format(movie[0],movie[1]))
                    result.append(movie)
            return result

        if option == 2:                             # and actor
            filtered = self.db.execute("SELECT TITLE, {} FROM movies ".format('[CAST]')).fetchall()
            result = []
            for movie in filtered:
                if string2 in movie[1]:
                    print("{}   {}".format(movie[0],movie[1]))
                    result.append(movie)
            return result

        if option == 5:                             # filter: movies with over 100M$ in Box Office earning
            box_office = self.db.execute("SELECT TITLE, BOX_OFFICE FROM movies").fetchall()
            new = []
            for t in box_office:                    # iterate through movies
                if t[1] == 'N/A':                   # if we have no information on Box Office ignore it
                    pass
                elif "tt" in t[1]:
                    pass
                else:
                    number = self.string_to_int(t[1])   # once we have integer representation of the string
                    if number > 100000000:              # we check if it is above the threshold and
                        new.append(t)                   # append new list with a tuple
            print("TITLE   BOX_OFFICE")
            for film in new:
                print("{}  {}".format(film[0], film[1]))
            return new

        if option == 3:                                 # filter: nominated for Oscar but did not win
            filtered = self.db.execute("SELECT TITLE, AWARDS FROM movies").fetchall()
            result = []
            for movie in filtered:                      # if 'won' and 'Oscar' in AWARDS column
                if 'Oscar' in movie[1]:
                    if not 'Won' in movie[1]:
                        result.append(movie)            # append tuple to  list
                        print("{}   {}".format(movie[0], movie[1]))
            return result

        if option == 4:                                 #filter: get percentage of nominations
            filtered = self.db.execute("SELECT TITLE, AWARDS FROM movies ").fetchall()
            result = []
            for movie in filtered:                                  # for each film
                won, nominated = self.won_nominated(movie[1])       # method return number of nominations and wins
                if won != 0 and nominated != 0:
                    percentage = won/nominated
                    if percentage >= 0.8:
                        print(movie[0] + " - " + str(int(percentage*100)))
                        result.append(movie)
            return result


    def won_nominated(self,desc):
        W_N = re.findall(r'\d+', desc)                  # find all numbers in a string
        W_N = [float(s) for s in W_N]                   # make the float type for later calculations ( just in case )
        won,nominated = 0,0
        if desc == 'N/A':
            return 0,0
        if 'Oscar' in desc:                             # the number of numbers differs  ,nominations in the database
            if 'Won' in desc:                           # are those times when film got nominated and did not win
                if 'Nominated' in desc:                 # therefore  nominations = wins + loses
                    won = W_N[0] + W_N[2]
                    nominated = W_N[1] + W_N[3] + W_N[0] + W_N[2]
                else:
                    won = W_N[0] + W_N[1]
                    nominated = W_N[2] + W_N[0] + W_N[1]
            else:
                won = W_N[1]
                nominated = W_N[0] + W_N[2] + W_N[1]
        else:
            if 'wins' in desc:
                if 'nominations' in desc:
                    won = W_N[0]
                    nominated = W_N[1] + W_N[0]
                    if len(W_N) == 3:
                        won = W_N[1]
                        nominated = W_N[1] + W_N[0] + W_N[2]
                else:
                    won = W_N[0]
                    nominated = W_N[0]
            else:
                won = 0
                nominated = W_N[0]
        return won, nominated
    def string_to_int(self, s):
        if s != 'N/A':              # makes a number out of sum of dollars
            s = s.replace(",", '')
            s = s.strip('$')
            return int(s)
    def compare(self,option,m1,m2):
        if option == 'BO':
            first = self.db.execute("SELECT TITLE, BOX_OFFICE FROM movies WHERE TITLE = '{}'".format(m1)).fetchall()
            second = self.db.execute("SELECT TITLE, BOX_OFFICE FROM movies WHERE TITLE = '{}'".format(m2)).fetchall()
            if self.string_to_int(first[0][1]) > self.string_to_int(second[0][1]):
                print(first[0][0]+"  "+first[0][1])
                return first[0]
            if self.string_to_int(first[0][1]) == self.string_to_int(second[0][1]):
                return [first[0],second[0]]
            else:
                print(second[0][0] + "  " + second[0][1])
                return second[0]
        if option == 'imdb':
            first = self.db.execute("SELECT TITLE, IMDB_RATING FROM movies WHERE TITLE = '{}'".format(m1)).fetchall()
            second = self.db.execute("SELECT TITLE, IMDB_RATING FROM movies WHERE TITLE = '{}'".format(m2)).fetchall()
            if first[0][1] > second[0][1]:
                print(str(first[0][0])+"  "+str(first[0][1]))
                return first[0]
            elif first[0][1] == second[0][1]:
                print(str(first[0][0])+"  "+str(first[0][1]))
                print(str(second[0][0]) + "  " + str(second[0][1]))
                return [first[0],second[0]]
            elif first[0][1] < second[0][1]:
                print(str(second[0][0]) + "  " + str(second[0][1]))
                return second[0]
        if option == 'awards_won':
            first = self.db.execute("SELECT TITLE, AWARDS FROM movies WHERE TITLE = '{}'".format(m1)).fetchall()
            second = self.db.execute("SELECT TITLE, AWARDS FROM movies WHERE TITLE = '{}'".format(m2)).fetchall()
            w1,n1 = self.won_nominated(first[0][1])
            w2,n2 = self.won_nominated(second[0][1])
            if w1>w2:
                print(first[0][0] + "  " + str(w1))
                return (first[0][0],w1)
            elif w1 == w2:
                print(first[0][0] + "  " + str(w1))
                print(second[0][0] + "  " + str(w2))
                return [(first[0][0], w1),(second[0][0],w2)]
            elif w1 < w2:
                print(second[0][0] + "  " + str(w2))
                return (second[0][0],w2)
        if option =='runtime':
            first = self.db.execute("SELECT TITLE, RUNTIME FROM movies WHERE TITLE = '{}'".format(m1)).fetchall()
            second = self.db.execute("SELECT TITLE, RUNTIME FROM movies WHERE TITLE = '{}'".format(m2)).fetchall()
            integers = []
            if first[0][1] != 'N/A':
                integers.append(int(first[0][1].strip(" min")))
            else:
                integers.append(0)
            if second[0][1]!= 'N/A':
                integers.append(int(second[0][1].strip(" min")))
            else:
                integers.append(0)
            if integers[0] > integers[1]:
                print(first[0][0] + "  " + first[0][1])
                return first
            elif integers[0] == integers[1]:
                print(first[0][0] + "  " + first[0][1])
                print(second[0][0] + "  " + second[0][1])
                return [first,second]
            elif integers[0] < integers[1]:
                print(second[0][0] + "  " + second[0][1])
                return second
    def highscores(self):
        rt_scores = self.db.execute("SELECT TITLE, RUNTIME FROM movies").fetchall()
        int1 = []
        for score in rt_scores:
            if score[1] == 'N/A':
                int1.append(0)
            else:
                int1.append(int(score[1].strip(" min")))
        max_index1 = int1.index(max(int1))
        print("Runtime: "+ rt_scores[max_index1][0] + "   " + rt_scores[max_index1][1])

        BO_scores = self.db.execute("SELECT TITLE, BOX_OFFICE FROM movies").fetchall()
        int2 = []
        for score in BO_scores:
            if score[1] == 'N/A':
                int2.append(0)
            elif 'tt' in score[1]:
                int2.append(0)
            else:
                int2.append(self.string_to_int(score[1]))
        max_index2 = int2.index(max(int2))
        print("Box Office earnings: " + BO_scores[max_index2][0] + "   " + BO_scores[max_index2][1])

        awards_scores = self.db.execute("SELECT TITLE, AWARDS FROM movies").fetchall()
        won,nominated = [],[]
        for score in awards_scores:
            w,n = self.won_nominated(score[1])
            won.append(w)
            nominated.append(n)
        max_index3 = won.index(max(won))
        print("Most awards won: "+awards_scores[max_index3][0] + "   " + str(int(max(won))))

        max_index4 = nominated.index(max(nominated))
        print("Most awards nominations: "+awards_scores[max_index4][0]+ "   " + str(int(max(nominated))))

        imdb_scores = self.db.execute("SELECT TITLE, IMDB_RATING FROM movies").fetchall()
        imdb = []
        for score in imdb_scores:
            imdb.append(score[1])
        max_index5 = imdb.index(max(imdb))
        print("Highest imdb rating: " + imdb_scores[max_index5][0] + "   " +
              str(imdb_scores[max_index5][1]))

        oscar_scores = self.db.execute("SELECT TITLE, AWARDS FROM movies").fetchall()
        oscars = []
        for score in oscar_scores:
            if 'Oscar' and 'Won' in score[1]:
                oscars.append(int(re.findall(r'\d+|$', score[1])[0]))
            else:
                oscars.append(0)
        max_index6 = oscars.index(max(oscars))
        print("Most oscars won: " + str(max(oscars)) + "   " + oscar_scores[max_index6][0])

        return [rt_scores[max_index1],BO_scores[max_index2],(awards_scores[max_index3][0],max(won)),
               (awards_scores[max_index4][0],max(nominated)),imdb_scores[max_index5],(oscar_scores[max_index6][0],max(oscars))]
    def add_movie(self,title):
        sel = self.db.execute("SELECT * FROM movies").fetchall()
        sql = '''INSERT INTO movies(ID,TITLE) VALUES (?,?)'''
        self.db.execute(sql,(len(sel),title))
